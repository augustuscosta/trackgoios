//
//  User.h
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 26/09/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, retain) NSString* token;

@end
