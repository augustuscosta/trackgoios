//
//  MapViewController.m
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 26/09/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//


#import "MapViewController.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "MapPoint.h"
#import "Utility.h"
#import "DeviceDetailViewController.h"
#import "RestKitConfigurationUtil.h"
#import "AppDelegate.h"


@interface MapViewController ()

@end

@implementation MapViewController
@synthesize logoutButton = _logoutButton;
@synthesize refreshButton = _refreshButton;
@synthesize mapView = _mapView;
@synthesize objectManager = _objectManager;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.title = @"Mapa";
    [self loadObjectsFromDataStore];
}

-(void)viewWillDisappear:(BOOL)animated{
    RestKitConfigurationUtil *configurationUtil = [[RestKitConfigurationUtil alloc] init];
    [configurationUtil initRestKit];
}

- (void)loadObjectsFromDataStore
{
    NSManagedObjectContext *moc = [RKManagedObjectStore defaultStore].persistentStoreManagedObjectContext;
    
    if (moc != nil) {
        
        NSEntityDescription *entityDescription = [NSEntityDescription
                                                  entityForName:@"Devices" inManagedObjectContext:moc];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSError *error;
        NSArray *array = [moc executeFetchRequest:request error:&error];
        if (array != nil){
            objects = [NSMutableArray arrayWithArray:array];
        }
    }
    
    [self setPinLocation:self.mapView];
}

- (void)loadData
{
    NSString *initialPath = @"mobile/vehicles/";
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"trackGoToken"];
    NSString *path = [initialPath stringByAppendingString:token];
    [[RKObjectManager sharedManager] getObjectsAtPath:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        RKLogInfo(@"Veículos successful");
        [self loadObjectsFromDataStore];
    }failure:^(RKObjectRequestOperation *operation, NSError *error) {
        RKLogError(@"Veículos failed with error: %@", error);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Nenhum veículo encontrado"
                                                        message:@""
                                                       delegate:nil
                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }];
}

- (void)viewDidUnload
{
    [self setLogoutButton:nil];
    [self setMapView:nil];
    [self setRefreshButton:nil];
    [super viewDidUnload];
}

- (IBAction)logoutPressed:(UIBarButtonItem *)sender {
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"trackGoToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self dismissModalViewControllerAnimated:YES];
    
    RKObjectManager *objectManager = [RKObjectManager sharedManager];
    NSError* error;
    [objectManager.managedObjectStore resetPersistentStores:&error];
}

- (IBAction)refreshPressed:(UIBarButtonItem *)sender {
    [self loadData];
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (void)showLoading:(BOOL)toShow{
    UIApplication* app = [UIApplication sharedApplication];
	if(toShow)
        app.networkActivityIndicatorVisible = YES;
    else
        app.networkActivityIndicatorVisible = NO;
}

#pragma mark MapView Delegate

-(void)setPinLocation : (MKMapView *)mapView{
    NSArray *ants = [mapView annotations];
    [mapView removeAnnotations:ants];
    
    for (Devices *devices in objects) {
        if ([devices lastPosition]) {
            CGFloat latDelta = [devices.lastPosition.latitude floatValue];
            CGFloat longDelta = [devices.lastPosition.longitude floatValue];
            CLLocationCoordinate2D newCoordinate = {latDelta, longDelta};
            MapPoint *mapPoint = [[MapPoint alloc] initWithCoordinate:newCoordinate title:devices.deviceName subtitle:devices.deviceDescription device:devices];
            [mapView addAnnotation:mapPoint];
        }
    }
    
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in mapView.annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
        if (MKMapRectIsNull(zoomRect)) {
            zoomRect = pointRect;
        } else {
            zoomRect = MKMapRectUnion(zoomRect, pointRect);
        }
    }
    [mapView setVisibleMapRect:zoomRect animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mv viewForAnnotation:(id <MKAnnotation>)annotation
{
    if([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    NSString *annotationIdentifier = @"PinViewAnnotation";
    
    MKPinAnnotationView *pinView = (MKPinAnnotationView *) [mv dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    if (!pinView)
    {
        pinView = [[MKPinAnnotationView alloc]
                   initWithAnnotation:annotation
                   reuseIdentifier:annotationIdentifier];
        
        [pinView setPinColor:MKPinAnnotationColorGreen];
        pinView.canShowCallout = YES;
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        [button setBackgroundImage:[UIImage imageNamed:@"bluerightarrow.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(openView) forControlEvents:UIControlEventTouchUpInside];
        pinView.rightCalloutAccessoryView = button;
    }
    else
    {
        pinView.annotation = annotation;
    }
    
    UIImage *pinImage;
    
    if ([[(MapPoint *)annotation device].lastPosition.panic isEqualToNumber:[NSNumber numberWithInt:1]])
        pinImage = [UIImage imageNamed:@"icone-vermelho.png"];
    else
        pinImage = [UIImage imageNamed:@"icone-verde.png"];
    
    pinView.image = pinImage;
    
    return pinView;
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    
    if([view.annotation isKindOfClass:[MKUserLocation class]])
        return;
    
    pinDevice = [(MapPoint *)view.annotation device];
}

-(void)openView{
    [self performSegueWithIdentifier:@"deviceDetailSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"deviceDetailSegue"]) {
        DeviceDetailViewController *ddvc = (DeviceDetailViewController *)segue.destinationViewController;
        ddvc.device = pinDevice;
    }
}

@end
