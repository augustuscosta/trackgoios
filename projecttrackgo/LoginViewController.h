//
//  LoginViewController.h
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 26/09/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"


@interface LoginViewController : UIViewController<UITextFieldDelegate, MBProgressHUDDelegate, UIAlertViewDelegate>{
    BOOL keyboardIsShown;
}


@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
@property (strong, nonatomic) IBOutlet UIImageView *poweredImageView;
@property (strong, nonatomic) IBOutlet UIScrollView *portraitView;

@property (strong, nonatomic) IBOutlet UIScrollView *landscapeView;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UITextField *loginTextField;

@property (strong, nonatomic) MBProgressHUD *HUD;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) UIImage *backgroundImageOriginal;
@property (strong, nonatomic) UIImage *backgroundImageResized;
@property (strong, nonatomic) UIImageView *backgroundImageView;
@end
