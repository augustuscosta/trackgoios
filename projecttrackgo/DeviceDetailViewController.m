//
//  DeviceDetailViewController.m
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 28/09/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "DeviceDetailViewController.h"
#import "CommandsViewController.h"

@interface DeviceDetailViewController ()

@end

@implementation DeviceDetailViewController
@synthesize tableView = _tableView;
@synthesize commandsButton;
@synthesize device = _device;
@synthesize num = _num;
@synthesize backgroundImageOriginal = _backgroundImageOriginal;
@synthesize backgroundImageResized = _backgroundImageResized;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSNumber *active = [NSNumber numberWithInt:1];
    
    NSString *code = self.device.code;
    NSString *detail = self.device.deviceDescription;
    NSString *deviceType = self.device.deviceType;
    NSString *name = self.device.deviceName;
    
    NSString *altitude = [NSString stringWithFormat:@"%@", self.device.lastPosition.altitude];
    NSString *date = [NSString stringWithFormat:@"%@", self.device.lastPosition.date];
    NSString *ignition = @"Desligada";
    NSLog(@"%@", self.device.lastPosition.ignition);
    if ([self.device.lastPosition.ignition isEqualToNumber:active]) {
        ignition = @"Ligada";
    }
    NSString *latitude = [NSString stringWithFormat:@"%@", self.device.lastPosition.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%@", self.device.lastPosition.longitude];
    NSString *panic = @"Desligado";
    if ([self.device.lastPosition.panic isEqualToNumber:active]) {
        panic = @"Ligado";
    }
    NSString *speed = [NSString stringWithFormat:@"%@", self.device.lastPosition.speed];

    leftDeviceLabelItems = [NSArray arrayWithObjects:@"Nome", @"Descrição", @"Rastreador", @"Modelo", nil];
    rightDeviceLabelItems = [NSArray arrayWithObjects:name,detail , code, deviceType, nil];
    leftLastPositionLabelItems = [NSArray arrayWithObjects:@"Latitude", @"Longitude", @"Velocidade", @"Ignição", @"Pânico", @"Data", @"Altitude", nil];
    rightLastPositionLabelItems = [NSArray arrayWithObjects:latitude, longitude, speed, ignition, panic, date, altitude, nil];
    
    headerLabel = [[UILabel alloc] init];
    headerLabel.font = [UIFont boldSystemFontOfSize:12.0];
    headerLabel.text = @"Testing";
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.backgroundImageOriginal = [UIImage imageNamed:@"bg.png"];
    self.backgroundImageResized = [self imageWithImage:self.backgroundImageOriginal convertToSize:self.view.frame.size];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:self.backgroundImageResized];
    [self.myTableView setBackgroundView:backgroundImageView];

}
- (IBAction)CommandsButtonPressed:(UIBarButtonItem *)sender {
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [self setCommandsButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    self.backgroundImageResized = [self imageWithImage:self.backgroundImageOriginal convertToSize:self.view.frame.size];
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30.0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    if (section == 0)
        return @"Dispositivo";
    else
        return @"Última Posição";
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    
    if (sectionTitle == nil) {
        return nil;
    }
    
    // Create label with section title
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 5.5f, 300.0f, 30.0f)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:18];
    label.textColor = [UIColor whiteColor];
    label.shadowColor = [UIColor blackColor];
    label.shadowOffset = CGSizeMake(0.0, 2.0);
    label.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    label.text = sectionTitle;
    
    // Create header view and add label as a subview
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 44.0f)];
    view.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    [view addSubview:label];
    
    return view;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0)
        return leftDeviceLabelItems.count;
    else
        return leftLastPositionLabelItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle
                                  reuseIdentifier:CellIdentifier];

    if (indexPath.section == 0) {
        cell.textLabel.text = [leftDeviceLabelItems objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [rightDeviceLabelItems objectAtIndex:indexPath.row];
    }
    
    if (indexPath.section == 1) {
        cell.textLabel.text = [leftLastPositionLabelItems objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [rightLastPositionLabelItems objectAtIndex:indexPath.row];
    }
    [[cell textLabel] setNumberOfLines:0]; // unlimited number of lines
    [[cell textLabel] setLineBreakMode:NSLineBreakByWordWrapping];
    
    [[cell detailTextLabel] setNumberOfLines:0]; // unlimited number of lines
    [[cell detailTextLabel] setLineBreakMode:NSLineBreakByWordWrapping];
    [[cell detailTextLabel] setFont:[UIFont systemFontOfSize: 14.0]];
    
    //[cell setBackgroundColor:[UIColor clearColor]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellTextTitle;
    NSString *cellTextSubtitle;
    UIFont *cellFont = [UIFont fontWithName:@"Helvetica" size:14.0];
    CGSize constraintSize = CGSizeMake(280.0f, MAXFLOAT);
    
    if (indexPath.section == 0) {
        cellTextTitle = [leftDeviceLabelItems objectAtIndex:indexPath.row];
        cellTextSubtitle = [rightDeviceLabelItems objectAtIndex:indexPath.row];
        
    }
    else{
        cellTextTitle = [leftLastPositionLabelItems objectAtIndex:indexPath.row];
        cellTextSubtitle = [rightLastPositionLabelItems objectAtIndex:indexPath.row];
    }

    
    CGSize titleLabelSize = [cellTextTitle sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
    CGSize subtitleLabelSize = [cellTextSubtitle sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat finalSize = titleLabelSize.height + subtitleLabelSize.height;
    
    return finalSize + 20;
    
}


#pragma mark - segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"CommandsSegue"]) {
        CommandsViewController *cvc = (CommandsViewController *)segue.destinationViewController;
        cvc.device = self.device;
    }
}


@end
