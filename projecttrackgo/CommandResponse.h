//
//  CommandResponse.h
//  projecttrackgo
//
//  Created by Augustus Costa on 1/30/14.
//  Copyright (c) 2014 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CommandResponse : NSManagedObject


@end
