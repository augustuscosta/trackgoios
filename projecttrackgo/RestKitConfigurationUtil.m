//
//  RestKitConfigurationUtil.m
//  skynet-version
//
//  Created by Nichollas Fonseca on 21/11/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "RestKitConfigurationUtil.h"

#import "Devices.h"
#import "LastPosition.h"
#import "Event.h"
#import "EventHistoryDetail.h"

NSString * const URL_BASE =  @"http://localhost:8080/trackgoweb/";


@implementation RestKitConfigurationUtil

-(void)initRestKit{
    RKLogConfigureByName("*", RKLogLevelTrace);
    
    // Initialize RestKit
    NSURL *baseURL = [NSURL URLWithString:URL_BASE];
    self.objectManager = [RKObjectManager managerWithBaseURL:baseURL];
    
    // Enable Activity Indicator Spinner
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    // Initialize managed object store
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    self.objectManager.managedObjectStore = managedObjectStore;
    
    [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"text/json"];
    
    [self initCoreData];
    
    RKObjectMapping *responseMappingDevices = [self getDevicesObjectMapping];
    [self.objectManager addResponseDescriptor:[RKResponseDescriptor responseDescriptorWithMapping:responseMappingDevices pathPattern:@"mobile/vehicles/:token" keyPath:@"devices" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)]];
    
    RKObjectMapping *responseMappingEvent = [self getEventHistoryDetailObjectMapping];
    [self.objectManager addResponseDescriptor:[RKResponseDescriptor responseDescriptorWithMapping:responseMappingEvent pathPattern:@"mobile/events/:token" keyPath:@"events" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)]];
    
    RKObjectMapping *responseMappingToken = [self getTokenMapping];
    [self.objectManager addResponseDescriptor:[RKResponseDescriptor responseDescriptorWithMapping:responseMappingToken pathPattern:@"mobile/session" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)]];
    
    
    RKObjectMapping *responseMappingCommand = [self getCommandResponseMapping];
    [self.objectManager addResponseDescriptor:[RKResponseDescriptor responseDescriptorWithMapping:responseMappingCommand pathPattern:@"mobile/comando/:comando" keyPath:nil statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)]];
    
    
}

-(RKObjectMapping *) getDevicesObjectMapping{
    RKEntityMapping * mapping = [RKEntityMapping mappingForEntityForName:@"Devices" inManagedObjectStore:self.objectManager.managedObjectStore];
    
    [mapping addAttributeMappingsFromDictionary:@{@"id":@"deviceId",
                                                  @"code":@"code",
                                                  @"name":@"deviceName",
                                                  @"description":@"deviceDescription",
                                                  @"enabled":@"deviceEnable",
                                                  @"deviceType":@"deviceType"}];
    
    
    mapping.identificationAttributes = @[@"deviceId"];
    
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"lastPosition" toKeyPath:@"lastPosition" withMapping:[self getLastPositionObjectMapping]]];
    
    return mapping;
}

-(RKObjectMapping *) getLastPositionObjectMapping{
    RKEntityMapping * mapping = [RKEntityMapping mappingForEntityForName:@"LastPosition" inManagedObjectStore:self.objectManager.managedObjectStore];
    [mapping addAttributeMappingsFromDictionary:@{@"latitude":@"latitude",
                                                  @"longitude":@"longitude",
                                                  @"altitude":@"altitude",
                                                  @"course":@"course",
                                                  @"speed":@"speed",
                                                  @"moving":@"moving",
                                                  @"date":@"date",
                                                  @"ignition":@"ignition",
                                                  @"panic":@"panic"}];
    
    return mapping;
}

-(RKObjectMapping *) getEventObjectMapping{
    RKEntityMapping * mapping = [RKEntityMapping mappingForEntityForName:@"Event" inManagedObjectStore:self.objectManager.managedObjectStore];
    [mapping addAttributeMappingsFromDictionary:@{@"id":@"eventId",
                                                  @"name":@"eventName",
                                                  @"description":@"eventDescription",
                                                  @"enable":@"eventEnable"}];
    
    mapping.identificationAttributes = @[@"eventId"];
    return mapping;
}

-(RKObjectMapping *) getEventHistoryDetailObjectMapping{
    RKEntityMapping * mapping = [RKEntityMapping mappingForEntityForName:@"EventHistoryDetail" inManagedObjectStore:self.objectManager.managedObjectStore];
    [mapping addAttributeMappingsFromDictionary:@{@"id":@"eventHistoryId",
                                                  @"metricDate":@"metricDate",
                                                  @"latitude":@"latitude",
                                                  @"longitude":@"longitude"}];
    
    mapping.identificationAttributes = @[@"eventHistoryId"];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"event" toKeyPath:@"event" withMapping:[self getEventObjectMapping]]];
    [mapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"device" toKeyPath:@"device" withMapping:[self getDevicesObjectMapping]]];
    return mapping;
}

-(RKObjectMapping *) getTokenMapping{
    RKEntityMapping * mapping = [RKEntityMapping mappingForEntityForName:@"Token" inManagedObjectStore:self.objectManager.managedObjectStore];
    [mapping addAttributeMappingsFromDictionary:@{@"token":@"token"}];
    
    return mapping;
}

-(RKObjectMapping *) getCommandResponseMapping{
    RKEntityMapping * mapping = [RKEntityMapping mappingForEntityForName:@"CommandResponse" inManagedObjectStore:self.objectManager.managedObjectStore];
    
    return mapping;
}



-(void)initCoreData{
    
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    self.objectManager.managedObjectStore = managedObjectStore;
    [managedObjectStore createPersistentStoreCoordinator];
    
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"trackgo.sqlite"];
    NSString *seedPath = [[NSBundle mainBundle] pathForResource:@"TrackgoSeedDateBbse" ofType:@"sqlite"];
    NSError *error;
    NSPersistentStore *persistentStore = [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:seedPath withConfiguration:nil options:nil error:&error];
    NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    
    // Create the managed object contexts
    [managedObjectStore createManagedObjectContexts];
    
    // Configure a managed object cache to ensure we do not create duplicate objects
    managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:self.objectManager.managedObjectStore.persistentStoreManagedObjectContext];
    
    // Set the default store shared instance
    [RKManagedObjectStore setDefaultStore:managedObjectStore];
}


@end
