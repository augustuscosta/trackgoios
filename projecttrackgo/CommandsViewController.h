//
//  CommandsViewController.h
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 03/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIDropDownMenu.h"
#import <RestKit/RestKit.h>
#import "Devices.h"

@interface CommandsViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource,UIGestureRecognizerDelegate>{
    NSMutableArray *searchWhereOptions;
    NSMutableArray *searchTimeZone;
    UIView *currentView;
    BOOL keyboardIsShown;
    NSInteger selectedOption;
    NSInteger selectedTimeZone;
    UIActionSheet *actionSheet;
    UIPickerView *myPickerView;
    UIToolbar *toolbar;
    UISegmentedControl *closeButton;
    BOOL pickerOnScreen;
}
@property (strong, nonatomic) IBOutlet UIView *timeZoneView;
@property (strong, nonatomic) IBOutlet UITextField *timeZoneTextfield;
@property (strong, nonatomic) IBOutlet UIButton *timeZoneButton;

@property (strong, nonatomic) IBOutlet UITextField *deviceName;
@property (strong, nonatomic) IBOutlet UITextField *deviceDescription;
@property (strong, nonatomic) IBOutlet UITextField *deviceTracker;
@property (strong, nonatomic) IBOutlet UITextField *deviceTrackerType;

@property (strong, nonatomic) IBOutlet UIView *outputView;
@property (strong, nonatomic) IBOutlet UIView *antiTheftView;
@property (strong, nonatomic) IBOutlet UIView *noParamView;
@property (strong, nonatomic) IBOutlet UIView *APNView;

@property (strong, nonatomic) IBOutlet UIView *serverConfigView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) IBOutlet UITextField *textfield;
@property (strong, nonatomic) IBOutlet UISwitch *exitOneSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *exitTwoSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *exitThreeSwitch;
@property (strong, nonatomic) IBOutlet UIButton *sendButton;
@property (strong, nonatomic) IBOutlet UISwitch *antiTheftSwitch;
@property (strong, nonatomic) IBOutlet UITextField *URLTextfield;
@property (strong, nonatomic) IBOutlet UITextField *userTextfield;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextfield;
@property (strong, nonatomic) IBOutlet UITextField *firstServer;
@property (strong, nonatomic) IBOutlet UITextField *firstPort;
@property (strong, nonatomic) IBOutlet UITextField *secondServer;
@property (strong, nonatomic) IBOutlet UITextField *secondPort;
@property (nonatomic, retain) NSArray *arrayView;
@property (nonatomic, strong) UIImage *backgroundImageOriginal;
@property (nonatomic, strong) UIImage *backgroundImageResized;
@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (strong, nonatomic) UIDropDownMenu *menu;
@property (nonatomic, retain) Devices *device;

@end
