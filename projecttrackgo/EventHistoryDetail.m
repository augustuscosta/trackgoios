//
//  EventHistoryDetail.m
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 01/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "EventHistoryDetail.h"
#import "Devices.h"
#import "Event.h"


@implementation EventHistoryDetail

@dynamic eventHistoryId;
@dynamic latitude;
@dynamic longitude;
@dynamic metricDate;
@dynamic event;
@dynamic device;

@end
