//
//  Event.m
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 01/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "Event.h"
#import "EventHistoryDetail.h"


@implementation Event

@dynamic eventDescription;
@dynamic eventEnable;
@dynamic eventId;
@dynamic eventName;
@dynamic eventHistoryDetail;

@end
