//
//  Utility.m
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 26/09/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "Utility.h"

@implementation Utility

+(BOOL)alreadyHasToken{
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"trackGoToken"];
    NSLog(@"%@", token);
    if (token == (id)[NSNull null] || token.length == 0 ){
        return NO;
    }
    return YES;
}

@end
