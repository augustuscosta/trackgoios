//
//  EventDetailMapViewController.h
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 02/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventHistoryDetail.h"
#import "Event.h"
#import "Devices.h"
#import <MapKit/MapKit.h>

@interface EventDetailMapViewController : UIViewController{
    EventHistoryDetail *pinEvent;
}

@property (nonatomic, retain) EventHistoryDetail *event;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@end
