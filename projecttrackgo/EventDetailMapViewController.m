//
//  EventDetailMapViewController.m
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 02/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "EventDetailMapViewController.h"
#import "MapPoint.h"
#import "EventDetailTableViewController.h"

@interface EventDetailMapViewController ()


@end

@implementation EventDetailMapViewController

@synthesize event = _event;
@synthesize mapView = _mapView;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setPinLocation:self.mapView];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    for (id<MKAnnotation> ann in self.mapView.selectedAnnotations) {
        [self.mapView deselectAnnotation:ann animated:NO];
    }
}

- (void)viewDidUnload
{
    [self setMapView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

-(void)setPinLocation : (MKMapView *)mapView{
    
    CGFloat latDelta = [self.event.latitude floatValue];
    CGFloat longDelta = [self.event.longitude floatValue];
    CLLocationCoordinate2D newCoordinate = {latDelta, longDelta};
    MapPoint *mapPoint = [[MapPoint alloc] initWithCoordinate:newCoordinate title:self.event.event.eventName subtitle:self.event.event.eventDescription event:self.event];
    [mapView addAnnotation:mapPoint];
    
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in mapView.annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
        if (MKMapRectIsNull(zoomRect)) {
            zoomRect = pointRect;
        } else {
            zoomRect = MKMapRectUnion(zoomRect, pointRect);
        }
    }
    [mapView setVisibleMapRect:zoomRect animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mv viewForAnnotation:(id <MKAnnotation>)annotation
{
    if([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    NSString *annotationIdentifier = @"PinViewAnnotation";
    
    MKPinAnnotationView *pinView = (MKPinAnnotationView *) [mv dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    if (!pinView)
    {
        pinView = [[MKPinAnnotationView alloc]
                   initWithAnnotation:annotation
                   reuseIdentifier:annotationIdentifier];
        
        pinView.canShowCallout = NO;
        
        UIImage *pinImage;
        pinImage = [UIImage imageNamed:@"ocorrencia_vermelho.png"];
        
        pinView.image = pinImage;
    }
    else
    {
        pinView.annotation = annotation;
    }
    
    return pinView;
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    
    if([view.annotation isKindOfClass:[MKUserLocation class]])
        return;
    
    pinEvent = [(MapPoint *)view.annotation event];
    [self performSegueWithIdentifier:@"EventDetailSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"EventDetailSegue"]) {
        EventDetailTableViewController *edtvc = (EventDetailTableViewController *)segue.destinationViewController;
        edtvc.event = pinEvent;
    }
}

@end
