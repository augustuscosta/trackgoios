//
//  DeviceDetailViewController.h
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 28/09/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Devices.h"
#import "LastPosition.h"
#import <RestKit/RestKit.h>

@interface DeviceDetailViewController : UITableViewController{
    NSArray *leftDeviceLabelItems;
    NSArray *rightDeviceLabelItems;
    NSArray *leftLastPositionLabelItems;
    NSArray *rightLastPositionLabelItems;
    UIImage *backgroundImageOriginal;
    UIImage *backgroundImageResized;
    UILabel *headerLabel;
}
@property (strong, nonatomic) IBOutlet UITableView *myTableView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *commandsButton;
@property (nonatomic, retain) Devices *device;
@property (strong, nonatomic)NSNumber *num;
@property (nonatomic, strong) UIImage *backgroundImageOriginal;
@property (nonatomic, strong) UIImage *backgroundImageResized;
@end
