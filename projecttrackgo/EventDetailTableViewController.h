//
//  EventDetailTableViewController.h
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 02/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventHistoryDetail.h"
#import "Event.h"
#import "Devices.h"

@interface EventDetailTableViewController : UITableViewController{
    NSArray *titleEventHistoryLabelItems;
    NSArray *descriptionEventHistoryLabelItems;
    NSArray *titleEventLabelItems;
    NSArray *descriptionEventLabelItems;
    NSArray *titleDeviceLabelItems;
    NSArray *descriptionDeviceLabelItems;
    UILabel *headerLabel;
}

@property (nonatomic, retain) EventHistoryDetail *event;
@property (strong, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic, strong) UIImage *backgroundImageOriginal;
@property (nonatomic, strong) UIImage *backgroundImageResized;

@end
