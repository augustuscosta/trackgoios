//
//  EventListViewController.m
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 01/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "EventListViewController.h"
#import "SimpleTableCell.h"
#import "Utility.h"
#import "EventDetailMapViewController.h"

@interface EventListViewController ()

@end

@implementation EventListViewController
@synthesize logoutButton = _logoutButton;
@synthesize refreshButton = _refreshButton;
@synthesize events = _events;
@synthesize backgroundImageOriginal = _backgroundImageOriginal;
@synthesize backgroundImageResized = _backgroundImageResized;
@synthesize myTableView = _myTableView;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.events = [[NSMutableArray alloc] init];
    [self loadObjectsFromDataStore];
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.backgroundImageOriginal = [UIImage imageNamed:@"bg.png"];
    self.backgroundImageResized = [self imageWithImage:self.backgroundImageOriginal convertToSize:self.view.frame.size];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:self.backgroundImageResized];
    [self.myTableView setBackgroundView:backgroundImageView];
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

- (void)loadObjectsFromDataStore
{
    NSManagedObjectContext *moc = [RKManagedObjectStore defaultStore].persistentStoreManagedObjectContext;
    
    if (moc != nil) {
        
        NSEntityDescription *entityDescription = [NSEntityDescription
                                                  entityForName:@"EventHistoryDetail" inManagedObjectContext:moc];
        NSSortDescriptor* descriptor = [NSSortDescriptor sortDescriptorWithKey:@"metricDate" ascending:NO];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        [request setSortDescriptors:[NSArray arrayWithObject:descriptor]];
        
        NSError *error;
        NSArray *array = [moc executeFetchRequest:request error:&error];
        if (array != nil){
             self.events = [NSMutableArray arrayWithArray:array];
        }
    }
    [self.tableView reloadData];
    
}

- (void)loadData
{
    NSString *initialPath = @"mobile/events/";
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"trackGoToken"];
    NSString *path = [initialPath stringByAppendingString:token];
    
    [[RKObjectManager sharedManager] getObject:nil path:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        RKLogInfo(@"Eventos successful");
        [self loadObjectsFromDataStore];
    }failure:^(RKObjectRequestOperation *operation, NSError *error) {
        RKLogError(@"Usuario failed with error: %@", error);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Nenhum evento encontrado"
                                                        message:@""
                                                       delegate:nil
                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }];
}
- (IBAction)refreshPressed:(UIBarButtonItem *)sender {
    [self loadData];
}

- (IBAction)logoutPressed:(UIBarButtonItem *)sender {
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"trackGoToken"];
    [self dismissModalViewControllerAnimated:YES];
    [Utility alreadyHasToken];
}

- (void)viewDidUnload
{
    [self setRefreshButton:nil];
    [self setLogoutButton:nil];
    [self setMyTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    self.backgroundImageResized = [self imageWithImage:self.backgroundImageOriginal convertToSize:self.view.frame.size];
}


#pragma mark - Table view data source


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    SimpleTableCell *cell = (SimpleTableCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SimpleTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
    }
    
    cell.eventName.text = [[[self.events objectAtIndex:indexPath.row] event] eventName];
    cell.eventName.font = [UIFont fontWithName:@"Helvetica" size:16];
    cell.eventDevice.text = [[[self.events objectAtIndex:indexPath.row] device] deviceName];
    cell.eventDevice.font = [UIFont fontWithName:@"Helvetica" size:16];
    
    NSDate *today = [[self.events objectAtIndex:indexPath.row] metricDate];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    
    cell.eventDate.text = [dateFormat stringFromDate:today];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row % 2)
    {
        [cell setBackgroundColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0]];
    }
    else [cell setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:244.0f/255.0f blue:244.0f/255.0f alpha:1.0]];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;  
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.events.count;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    eventSelected = [self.events objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"EventOnMapSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"EventOnMapSegue"]) {
        EventDetailMapViewController *edmvc = (EventDetailMapViewController *)segue.destinationViewController;
        NSLog(@"%@", [eventSelected description]);
        edmvc.event = eventSelected;
    }
}

@end
