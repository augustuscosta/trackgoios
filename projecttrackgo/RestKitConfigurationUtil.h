//
//  RestKitConfigurationUtil.h
//  skynet-version
//
//  Created by Nichollas Fonseca on 21/11/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>

@interface RestKitConfigurationUtil : NSObject

extern NSString * const URL_BASE;
extern NSString * const STORE_PATH;
extern NSString * const SEED_PATH;

@property (nonatomic, retain) RKObjectManager * objectManager;

-(void)initRestKit;
-(void)initCoreData;

@end
