//
//  Devices.h
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 01/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class EventHistoryDetail, LastPosition;

@interface Devices : NSManagedObject

@property (nonatomic, retain) NSString * code;
@property (nonatomic, retain) NSString * deviceDescription;
@property (nonatomic, retain) NSString * deviceType;
@property (nonatomic, retain) NSNumber * deviceEnable;
@property (nonatomic, retain) NSNumber * deviceId;
@property (nonatomic, retain) NSString * deviceName;
@property (nonatomic, retain) LastPosition *lastPosition;
@property (nonatomic, retain) EventHistoryDetail *eventHistoryDetail;

@end
