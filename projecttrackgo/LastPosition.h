//
//  LastPosition.h
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 01/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Devices;

@interface LastPosition : NSManagedObject

@property (nonatomic, retain) NSNumber * altitude;
@property (nonatomic, retain) NSNumber * course;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * ignition;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSNumber * moving;
@property (nonatomic, retain) NSNumber * panic;
@property (nonatomic, retain) NSNumber * speed;
@property (nonatomic, retain) Devices *device;

@end
