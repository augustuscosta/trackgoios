//
//  EventListViewController.h
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 01/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "EventHistoryDetail.h"
#import "Devices.h"
#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>

@interface EventListViewController : UITableViewController{
    EventHistoryDetail *eventSelected;
    UIImage *backgroundImageOriginal;
    UIImage *backgroundImageResized;

}
@property (strong, nonatomic) IBOutlet UITableView *myTableView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *logoutButton;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *refreshButton;
@property (strong, nonatomic)NSMutableArray *events;
@property (nonatomic, strong) UIImage *backgroundImageOriginal;
@property (nonatomic, strong) UIImage *backgroundImageResized;
@end
