//
//  MapPoint.m
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 28/09/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "MapPoint.h"

@implementation MapPoint

@synthesize title = _title;
@synthesize subtitle = _subtitle;
@synthesize coordinate = _coordinate;
@synthesize device = _device;
@synthesize event = _event;

-(id) initWithCoordinate:(CLLocationCoordinate2D)c title:(NSString *)t subtitle:(NSString *)sub device:(Devices *)d{
    self = [super init];
    if (self != nil) {
        _coordinate = c;
        _title = t;
        _subtitle = sub;
        _device = d;
        _event = nil;
    }
    return self;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D)c title:(NSString *)t subtitle:(NSString *)sub event:(EventHistoryDetail *)e{
    self = [super init];
    if (self != nil) {
        _coordinate = c;
        _title = t;
        _subtitle = sub;
        _device = nil;
        _event = e;
    }
    return self;
}

@end
