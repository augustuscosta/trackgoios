//
//  CommandsViewController.m
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 03/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "CommandsViewController.h"

@interface CommandsViewController ()

@end

@implementation CommandsViewController
@synthesize antiTheftView = _antiTheftView;
@synthesize noParamView = _noParamView;
@synthesize APNView = _APNView;
@synthesize serverConfigView = _serverConfigView;
@synthesize deviceName = _deviceName;
@synthesize deviceDescription = _deviceDescription;
@synthesize deviceTrackerType = _deviceTrackerType;
@synthesize outputView = _outputView;
@synthesize scrollView = _scrollView;
@synthesize textfield = _textfield;
@synthesize exitOneSwitch = _exitOneSwitch;
@synthesize exitTwoSwitch = _exitTwoSwitch;
@synthesize exitThreeSwitch = _exitThreeSwitch;
@synthesize sendButton = _sendButton;
@synthesize antiTheftSwitch = _antiTheftSwitch;
@synthesize URLTextfield = _URLTextfield;
@synthesize userTextfield = _userTextfield;
@synthesize passwordTextfield = _passwordTextfield;
@synthesize firstServer = _firstServer;
@synthesize firstPort = _firstPort;
@synthesize secondServer = _secondServer;
@synthesize secondPort = _secondPort;
@synthesize button = _button;
@synthesize menu = _menu;
@synthesize backgroundImageView = _backgroundImageView;
@synthesize backgroundImageResized = _backgroundImageResized;
@synthesize backgroundImageOriginal = _backgroundImageOriginal;
@synthesize arrayView = _arrayView;
@synthesize device = _device;

#define kbHeight 216.0

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.antiTheftSwitch.on = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    keyboardIsShown = NO;
    
    searchWhereOptions = [[NSMutableArray alloc] initWithObjects:@"Requerer Posição", @"Desabilitar Pânico", @"Controlar Anti furto", @"Desabilitar anti furto", @"Mudar fuso horário", @"Ativar Saídas", @"Mudar APN do rastreador", @"Mudar configurações do servidor", @"Resetar configurações do servidor", nil];
    searchTimeZone = [[NSMutableArray alloc] initWithObjects:@"-12:00", @"-11:00", @"-10:00", @"-9:00", @"-8:00", @"-7:00", @"-6:00", @"-5:00", @"-4:00", @"-3:00", @"-2:00", @"-1:00", @"0:00", @"1:00", @"2:00", @"3:00", @"4:00", @"5:00", @"6:00", @"7:00", @"8:00", @"9:00", @"10:00", @"11:00", @"12:00", @"13:00", nil];
    
    // default list option to select
    
    selectedOption = 0;
    selectedTimeZone = 0;
    
    self.arrayView = [[NSArray alloc] initWithObjects:self.noParamView, self.noParamView, self.antiTheftView, self.noParamView,self.timeZoneView, self.outputView, self.APNView, self.serverConfigView, self.noParamView, nil];
    
    for (UIView *view in self.arrayView) {  
        [view setAlpha:0];
    }
    
    self.deviceName.text = self.device.deviceName;
    self.deviceDescription.text = self.device.deviceDescription;
    self.deviceTracker.text = self.device.code;
    self.deviceTrackerType.text = self.device.deviceType;

    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.scrollView.contentSize = self.view.frame.size;
    
    
    
    self.backgroundImageOriginal = [UIImage imageNamed:@"bg.png"];
    self.backgroundImageResized = [self imageWithImage:self.backgroundImageOriginal convertToSize:self.view.frame.size];
    self.backgroundImageView = [[UIImageView alloc] initWithImage:self.backgroundImageResized];
    [self.view addSubview:self.backgroundImageView];
    [self.view sendSubviewToBack:self.backgroundImageView];
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [self setButton:nil];
    [self setTextfield:nil];
    [self setAntiTheftView:nil];
    [self setOutputView:nil];
    [self setAPNView:nil];
    [self setServerConfigView:nil];
    [self setNoParamView:nil];
    [self setExitOneSwitch:nil];
    [self setExitTwoSwitch:nil];
    [self setExitThreeSwitch:nil];
    [self setSendButton:nil];
    [self setAntiTheftSwitch:nil];
    [self setURLTextfield:nil];
    [self setUserTextfield:nil];
    [self setPasswordTextfield:nil];
    [self setFirstServer:nil];
    [self setFirstPort:nil];
    [self setSecondServer:nil];
    [self setSecondPort:nil];
    [self setDeviceName:nil];
    [self setDeviceDescription:nil];
    [self setDeviceTrackerType:nil];
    [self setTimeZoneView:nil];
    [self setTimeZoneTextfield:nil];
    [self setTimeZoneButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [self.backgroundImageView removeFromSuperview];
    
    self.backgroundImageResized = [self imageWithImage:self.backgroundImageOriginal convertToSize:self.view.frame.size];
    self.backgroundImageView = [[UIImageView alloc] initWithImage:self.backgroundImageResized];
    [self.view addSubview:self.backgroundImageView];
    [self.view sendSubviewToBack:self.backgroundImageView];
    
    CGSize scrollSize;
    scrollSize = CGSizeMake(self.scrollView.contentSize.width, 147 + currentView.bounds.size.height);
    if (self.view.frame.size.height < self.scrollView.contentSize.height) {
        self.scrollView.contentSize = scrollSize;
    }
    else
        self.scrollView.contentSize = self.view.frame.size;
    
    CGRect pickerFrame = CGRectMake(0,self.view.frame.size.height - 220, self.view.frame.size.width, 180);
    [myPickerView setFrame:pickerFrame];
    toolbar.frame = CGRectMake(0, self.view.frame.size.height - 40,self.view.frame.size.width , 40);
    closeButton.frame = CGRectMake(self.view.frame.size.width - 57, 7.0f, 50.0f, 30.0f);
    
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}



- (IBAction)showSearchWhereOptions:(UIButton *)sender {
    
    if (pickerOnScreen)
        return;
    
    CGRect pickerFrame = CGRectMake(0,self.view.frame.size.height - 220, self.view.frame.size.width, 180);
    
    myPickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    myPickerView.showsSelectionIndicator = YES;
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    
    [myPickerView selectRow:selectedOption inComponent:0 animated:NO];
    [myPickerView setTag:0];

    toolbar = [[UIToolbar alloc] init];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    toolbar.frame = CGRectMake(0, self.view.frame.size.height - 40,self.view.frame.size.width , 40);
    closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Ok"]];
    closeButton.momentary = YES;
    closeButton.frame = CGRectMake(self.view.frame.size.width - 57, 7.0f, 50.0f, 30.0f);
    closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
    closeButton.tintColor = [UIColor blackColor];
    [closeButton addTarget:self action:@selector(dismissPickerView) forControlEvents:UIControlEventValueChanged];
    [toolbar addSubview:closeButton];
    
    [self.view addSubview:toolbar];
    [self.view addSubview:myPickerView];
    
    
    if (selectedOption == 0){
        [[self.arrayView objectAtIndex:0] setAlpha:1];
        self.textfield.text = [searchWhereOptions objectAtIndex:0];
    }
    
    pickerOnScreen = TRUE;
    
}

- (IBAction)showSearchTimeZone:(UIButton *)sender {
    
    if (pickerOnScreen)
        return;
    
    CGRect pickerFrame = CGRectMake(0,self.view.frame.size.height - 220, self.view.frame.size.width, 180);
    
    myPickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    [myPickerView setTag:1];
    myPickerView.showsSelectionIndicator = YES;
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    
    [myPickerView selectRow:selectedTimeZone inComponent:0 animated:NO];
    
    
    toolbar = [[UIToolbar alloc] init];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    toolbar.frame = CGRectMake(0, self.view.frame.size.height - 40,self.view.frame.size.width , 40);
    closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Ok"]];
    closeButton.momentary = YES;
    closeButton.frame = CGRectMake(self.view.frame.size.width - 57, 7.0f, 50.0f, 30.0f);
    closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
    closeButton.tintColor = [UIColor blackColor];
    [closeButton addTarget:self action:@selector(dismissPickerView) forControlEvents:UIControlEventValueChanged];
    [toolbar addSubview:closeButton];
    
    [self.view addSubview:toolbar];
    [self.view addSubview:myPickerView];
    
    
    if (selectedTimeZone == 0){
        self.timeZoneTextfield.text = [searchTimeZone objectAtIndex:0];
    }
    
    pickerOnScreen = TRUE;
    
}

-(void)dismissPickerView{
    [myPickerView removeFromSuperview];
    [toolbar removeFromSuperview];
    pickerOnScreen = FALSE;
}

#pragma mark keyboardNotifications methods

- (void)keyboardWillHide:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    // resize the scrollview
    CGRect viewFrame = self.scrollView.frame;
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
        viewFrame.size.height += (keyboardSize.height);
    else
        viewFrame.size.height += (keyboardSize.width);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    // The kKeyboardAnimationDuration I am using is 0.3
    [UIView setAnimationDuration:0.3];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
    keyboardIsShown = NO;
}

- (void)keyboardWillShow:(NSNotification *)n
{
    if (keyboardIsShown) {
        return;
    }
    [self dismissPickerView];
    
    NSDictionary* userInfo = [n userInfo];
    
    [self.view bringSubviewToFront:self.scrollView];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // resize the noteView
    CGRect viewFrame = self.scrollView.frame;
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
        viewFrame.size.height -= (keyboardSize.height);
    else
        viewFrame.size.height -= (keyboardSize.width);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    // The kKeyboardAnimationDuration I am using is 0.3
    [UIView setAnimationDuration:0.3];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
    [self.scrollView setContentOffset:CGPointMake (0, 0) animated: NO];
    self.scrollView.contentSize = self.view.frame.size;
    
    keyboardIsShown = YES;
    if(self.URLTextfield.isFirstResponder)
        [self.scrollView setContentOffset:CGPointMake(0, (self.URLTextfield.frame.origin.y - self.URLTextfield.frame.size.height/2)/2 + 147) animated: NO];
    else if(self.userTextfield.isFirstResponder)
        [self.scrollView setContentOffset:CGPointMake(0, (self.userTextfield.frame.origin.y - self.userTextfield.frame.size.height/2)/2 + 147) animated: NO];
    else if(self.passwordTextfield.isFirstResponder)
        [self.scrollView setContentOffset:CGPointMake(0, (self.passwordTextfield.frame.origin.y - self.passwordTextfield.frame.size.height/2)/2 + 147) animated: NO];
    else if(self.firstServer.isFirstResponder)
        [self.scrollView setContentOffset:CGPointMake(0, (self.firstServer.frame.origin.y - self.firstServer.frame.size.height/2)/2 + 147) animated: NO];
    else if(self.firstPort.isFirstResponder)
        [self.scrollView setContentOffset:CGPointMake(0, (self.firstPort.frame.origin.y - self.firstPort.frame.size.height/2)/2 + 147) animated: NO];
    else if(self.secondServer.isFirstResponder)
        [self.scrollView setContentOffset:CGPointMake(0, (self.secondServer.frame.origin.y - self.secondServer.frame.size.height/2)/2 + 147) animated: NO];
    else if(self.secondPort.isFirstResponder)
        [self.scrollView setContentOffset:CGPointMake(0, (self.secondPort.frame.origin.y - self.secondPort.frame.size.height/2)/2 + 147) animated: NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
     
    return YES;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
    
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView.tag == 0)
        return [searchWhereOptions count];
    else
        return [searchTimeZone count];
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (pickerView.tag == 0)
        return [searchWhereOptions objectAtIndex:row];
    else
        return [searchTimeZone objectAtIndex:row];
}


// this method runs whenever the user changes the selected list option

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (pickerView.tag == 0) {
        self.textfield.text = [searchWhereOptions objectAtIndex:row];
        
        for (int i = 0;i<self.arrayView.count; i++) {
            [[self.arrayView objectAtIndex:i] setAlpha:0];
        }
        [[self.arrayView objectAtIndex:row] setAlpha:1];
        currentView = [self.arrayView objectAtIndex:row];
        CGSize scrollSize;
        scrollSize = CGSizeMake(self.scrollView.contentSize.width, 147 + currentView.bounds.size.height);
        if (scrollSize.height > self.scrollView.contentSize.height) {
            self.scrollView.contentSize = scrollSize;
        }
        else
            self.scrollView.contentSize = self.view.frame.size;
        
        selectedOption = row;
    }
    else{
        self.timeZoneTextfield.text = [searchTimeZone objectAtIndex:row];
        selectedTimeZone = row;
    }
}

- (IBAction)handleSendButton:(UIButton *)sender {
    NSMutableDictionary *params = [[ NSMutableDictionary alloc] init];
    NSNumber *deviceId = self.device.deviceId;
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"trackGoToken"];
    [ params setObject:deviceId forKey:@"device"];
    [ params setObject:token forKey:@"token"];
    
    if ([self.textfield.text isEqualToString:[searchWhereOptions objectAtIndex:0]]) {
        NSString *path = @"mobile/comando/request_position";
        [self doPost:path parameters:params];
    }
    if ([self.textfield.text isEqualToString:[searchWhereOptions objectAtIndex:1]]) {
        NSString *path = @"mobile/comando/disable_panic";
        [self doPost:path parameters:params];
    }
    if ([self.textfield.text isEqualToString:[searchWhereOptions objectAtIndex:2]]) {
        NSString *path = @"mobile/comando/anti_theft";
        NSNumber *enabled = [NSNumber numberWithBool: self.antiTheftSwitch.on];
        [params setObject:enabled forKey:@"enabled"];
        
        [self doPost:path parameters:params];
    }
    if ([self.textfield.text isEqualToString:[searchWhereOptions objectAtIndex:3]]) {
        NSString *path = @"mobile/comando/disable_anti_theft";
        [self doPost:path parameters:params];
    }
    if ([self.textfield.text isEqualToString:[searchWhereOptions objectAtIndex:4]]) {
        NSString *path = @"mobile/comando/time_zone";
        NSLog(@"%d", selectedTimeZone);
        NSNumber *timeZone = [NSNumber numberWithInt:(selectedTimeZone +1)];
        [params setObject:timeZone forKey:@"timeZone"];
        [self doPost:path parameters:params];
    }
    if ([self.textfield.text isEqualToString:[searchWhereOptions objectAtIndex:5]]) {
        NSString *path = @"mobile/comando/output";
        NSNumber *output1 = [NSNumber numberWithBool:self.exitOneSwitch.on];
        NSNumber *output2 = [NSNumber numberWithBool:self.exitTwoSwitch.on];
        NSNumber *output3 = [NSNumber numberWithBool:self.exitThreeSwitch.on];
        [params setObject:output1 forKey:@"output1"];
        [params setObject:output2 forKey:@"output2"];
        [params setObject:output3 forKey:@"output3"];
        [self doPost:path parameters:params];
    }
    if ([self.textfield.text isEqualToString:[searchWhereOptions objectAtIndex:6]]) {
        NSString *path = @"mobile/comando/apn";
        NSString *url = self.URLTextfield.text;
        NSString *username = self.userTextfield.text;
        NSString *password = self.passwordTextfield.text;
        [params setObject:url forKey:@"url"];
        [params setObject:username forKey:@"username"];
        [params setObject:password forKey:@"password"];
        [self doPost:path parameters:params];
        self.URLTextfield.text = self.userTextfield.text = self.passwordTextfield.text = @"";
        
    }
    if ([self.textfield.text isEqualToString:[searchWhereOptions objectAtIndex:7]]) {
        NSString *path = @"mobile/comando/server";
        NSString *primaryIp = self.firstServer.text;
        NSString *primaryPort = self.firstPort.text;
        NSString *seconderyIp = self.secondServer.text;
        NSString *seconderyPort = self.secondPort.text;
        [params setObject:primaryIp forKey:@"primaryIp"];
        [params setObject:primaryPort forKey:@"primaryPort"];
        [params setObject:seconderyIp forKey:@"seconderyIp"];
        [params setObject:seconderyPort forKey:@"seconderyPort"];
        [self doPost:path parameters:params];
        self.firstServer.text = self.firstPort.text = self.secondServer.text = self.secondPort.text = @"";
    }
    if ([self.textfield.text isEqualToString:[searchWhereOptions objectAtIndex:8]]) {
        NSString *path = @"/mobile/comando/reset";
        [self doPost:path parameters:params];
    }
}

-(void)doPost:(NSString *)path parameters:(NSMutableDictionary *)params{
    [[RKObjectManager sharedManager] postObject:nil path:path parameters:params success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Comando Enviado com Sucesso!"
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        if(error.code == -1009){
            
            [[[UIAlertView alloc] initWithTitle:@"Sem conexão"
                                        message:@"Conecte-se a internet para efetuar o login"
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        }else{
            
            [[[UIAlertView alloc] initWithTitle:@"Erro"
                                        message:@"Erro ao enviar comando"
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        }
    }];
}




-(void)showAllertAndDismissModalViewController{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sessão Expirada"
                                                    message:@"Sua sessão foi expirada e será encerrada."
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles: nil];
    [alert setTag:401];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex {
    if(alertView.tag == 401){
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"trackGoToken"];
        [self dismissModalViewControllerAnimated:YES];
    }
}


@end
