//
//  Event.h
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 01/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class EventHistoryDetail;

@interface Event : NSManagedObject

@property (nonatomic, retain) NSString * eventDescription;
@property (nonatomic, retain) NSNumber * eventEnable;
@property (nonatomic, retain) NSNumber * eventId;
@property (nonatomic, retain) NSString * eventName;
@property (nonatomic, retain) EventHistoryDetail *eventHistoryDetail;

@end
