//
//  LoginViewController.m
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 26/09/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "LoginViewController.h"
#import "User.h"
#import "Token.h"
#import "Utility.h"
#import <RestKit/RestKit.h>


static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

CGFloat animatedDistance;

@interface LoginViewController ()

@end

@implementation LoginViewController
@synthesize logoImageView = _logoImageView;
@synthesize poweredImageView = _poweredImageView;
@synthesize portraitView = _portraitView;
@synthesize landscapeView = _landscapeView;
@synthesize loginButton = _loginButton;
@synthesize loginTextField = _loginTextField;
@synthesize passwordTextField = _passwordTextField;
@synthesize scrollView = _scrollView;
@synthesize HUD = _HUD;
@synthesize backgroundImageOriginal = _backgroundImageOriginal;
@synthesize backgroundImageResized = _backgroundImageResized;
@synthesize backgroundImageView = _backgroundImageView;

-(void)loadView{
    [super loadView];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleBlackTranslucent;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    if ([Utility alreadyHasToken]) {
        [self performSegueWithIdentifier:@"loginMainSegue" sender:self];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    keyboardIsShown = NO;
    
    self.loginTextField.delegate = self;
    self.navigationController.navigationBarHidden = YES;
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.scrollView.contentSize = self.view.frame.size;
    
    [self.backgroundImageView removeFromSuperview];
    [self.HUD removeFromSuperview];
    
    self.backgroundImageOriginal = [UIImage imageNamed:@"bg.png"];
    self.backgroundImageResized = [self imageWithImage:self.backgroundImageOriginal convertToSize:self.scrollView.frame.size];
    self.backgroundImageView = [[UIImageView alloc] initWithImage:self.backgroundImageResized];
    [self.scrollView addSubview:self.backgroundImageView];
    [self.scrollView sendSubviewToBack:self.backgroundImageView];
    
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    
    
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
        [self setElementsPortrait];
    else
        [self setElementsLandscape];
}

-(void)setElementsPortrait{
    if (NSClassFromString(@"UISplitViewController") != nil && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        CGRect poweredImageFrame = self.poweredImageView.frame;
        poweredImageFrame.origin.x = 314;
        poweredImageFrame.origin.y = 930;
        [self.poweredImageView setFrame:poweredImageFrame];
    }
    else {
        CGRect logoImageFrame = self.logoImageView.frame;
        logoImageFrame.origin.x = 30;
        logoImageFrame.origin.y = 51;
        [self.logoImageView setFrame:logoImageFrame];
        CGRect loginTextfieldFrame = self.loginTextField.frame;
        loginTextfieldFrame.origin.x = 60;
        loginTextfieldFrame.origin.y = 159;
        [self.loginTextField setFrame:loginTextfieldFrame];
        CGRect passwordTextfieldFrame = self.loginTextField.frame;
        passwordTextfieldFrame.origin.x = 60;
        passwordTextfieldFrame.origin.y = 198;
        [self.passwordTextField setFrame:passwordTextfieldFrame];
        CGRect loginButtonFrame = self.loginButton.frame;
        loginButtonFrame.origin.x = 110;
        loginButtonFrame.origin.y = 269;
        [self.loginButton setFrame:loginButtonFrame];
        CGRect poweredImageFrame = self.poweredImageView.frame;
        poweredImageFrame.origin.x = 120;
        poweredImageFrame.origin.y = 421;
        [self.poweredImageView setFrame:poweredImageFrame];
    }
}

-(void)setElementsLandscape{
    if (NSClassFromString(@"UISplitViewController") != nil && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        CGRect poweredImageFrame = self.poweredImageView.frame;
        poweredImageFrame.origin.x = 441;
        poweredImageFrame.origin.y = 690;
        [self.poweredImageView setFrame:poweredImageFrame];
        
    }
    else{
        CGRect logoImageFrame = self.logoImageView.frame;
        logoImageFrame.origin.x = 110;
        logoImageFrame.origin.y = 20;
        [self.logoImageView setFrame:logoImageFrame];
        CGRect loginTextfieldFrame = self.loginTextField.frame;
        loginTextfieldFrame.origin.x = 90;
        loginTextfieldFrame.origin.y = 115;
        [self.loginTextField setFrame:loginTextfieldFrame];
        CGRect passwordTextfieldFrame = self.loginTextField.frame;
        passwordTextfieldFrame.origin.x = 90;
        passwordTextfieldFrame.origin.y = 154;
        [self.passwordTextField setFrame:passwordTextfieldFrame];
        CGRect loginButtonFrame = self.loginButton.frame;
        loginButtonFrame.origin.x = 165;
        loginButtonFrame.origin.y = 211;
        [self.loginButton setFrame:loginButtonFrame];
        CGRect poweredImageFrame = self.poweredImageView.frame;
        poweredImageFrame.origin.x = 200;
        poweredImageFrame.origin.y = 261;
        [self.poweredImageView setFrame:poweredImageFrame];
    }
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

-(void)loadWithLabel{
    
    self.HUD = [[MBProgressHUD alloc] initWithFrame:self.view.frame];
    [self.view addSubview:self.HUD];
    self.HUD.delegate = self;
    self.HUD.labelText = @"Logando...";
    [self.HUD show:YES];
    [self handleLogin];
}

- (void)handleLogin{
    NSMutableDictionary *params = [[ NSMutableDictionary alloc] init];
    NSString *username = self.loginTextField.text;
    NSString *password = self.passwordTextField.text;
    [ params setObject:username forKey:@"username"];
    [ params setObject:password forKey:@"password"];
    [[RKObjectManager sharedManager] postObject:nil path:@"mobile/session" parameters:params success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        RKLogInfo(@"AccessToken Recebido.");
        [self.HUD show:NO];
        [self.HUD removeFromSuperview];
        
        NSManagedObjectContext *moc = [RKManagedObjectStore defaultStore].persistentStoreManagedObjectContext;
        
        if (moc != nil) {
            
            NSEntityDescription *entityDescription = [NSEntityDescription
                                                      entityForName:@"Token" inManagedObjectContext:moc];
            NSFetchRequest *request = [[NSFetchRequest alloc] init];
            [request setEntity:entityDescription];
            
            NSError *error;
            NSArray *array = [moc executeFetchRequest:request error:&error];
            if (array != nil){
                Token* token = [array lastObject];
                NSString* tokenString = token.token;
                [[NSUserDefaults standardUserDefaults] setObject:tokenString forKey:@"trackGoToken"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                self.passwordTextField.text = @"";
                [self performSegueWithIdentifier:@"loginMainSegue" sender:self];
                
            }
        }
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [self.HUD show:NO];
        [self.HUD removeFromSuperview];
        
        if(error.code == -1009){
            
            [[[UIAlertView alloc] initWithTitle:@"Sem conexão"
                                        message:@"Conecte-se a internet para efetuar o login"
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        }if(error.code == -1011){
            
            [[[UIAlertView alloc] initWithTitle:@"Dados inválidos"
                                        message:@"Usuario ou senha incorretas"
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        }
        RKLogError(@"AccessToken failed with error: %@", error);
    }];
}

- (IBAction)doneClicked:(UIButton *)sender {
    
    if([self.loginTextField.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro ao logar"
                                                        message:@"Não foi possível fazer login com o servidor. O campo Usuário encontra-se vazio."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        return;
    }
    if ([self.passwordTextField.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erro ao logar"
                                                        message:@"Não foi possível fazer login com o servidor. O campo Senha encontra-se vazio."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        return;
    }
    [self loadWithLabel];
    
}

- (void)viewDidUnload
{
    [self setLoginTextField:nil];
    [self setPasswordTextField:nil];
    [self setLoginTextField:nil];
    [self setPasswordTextField:nil];
    [self setScrollView:nil];
    [self setLoginButton:nil];
    [self setLoginButton:nil];
    [self setLandscapeView:nil];
    [self setPortraitView:nil];
    [self setLogoImageView:nil];
    [self setPoweredImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

#pragma mark keyboardNotifications methods

- (void)keyboardWillHide:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    // resize the scrollview
    CGRect viewFrame = self.scrollView.frame;
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
        viewFrame.size.height += (keyboardSize.height);
    else
        viewFrame.size.height += (keyboardSize.width);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    // The kKeyboardAnimationDuration I am using is 0.3
    [UIView setAnimationDuration:0.3];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
    keyboardIsShown = NO;
}

- (void)keyboardWillShow:(NSNotification *)n
{
    if (keyboardIsShown) {
        return;
    }
    
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // resize the noteView
    CGRect viewFrame = self.scrollView.frame;
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
        viewFrame.size.height -= (keyboardSize.height);
    else
        viewFrame.size.height -= (keyboardSize.width);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    // The kKeyboardAnimationDuration I am using is 0.3
    [UIView setAnimationDuration:0.3];
    [self.scrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
    [self.scrollView setContentOffset:CGPointMake (0, 0) animated: NO];
    
    keyboardIsShown = YES;
    if(self.loginTextField.isFirstResponder)
        [self.scrollView setContentOffset:CGPointMake(0, (self.loginTextField.frame.origin.y - self.loginTextField.bounds.size.height/2)/2) animated: NO];
    else
        [self.scrollView setContentOffset:CGPointMake(0, (self.passwordTextField.frame.origin.y - self.passwordTextField.bounds.size.height/2)/2) animated: NO];
}

#pragma mark Rotate methods

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    self.scrollView.contentSize = self.view.frame.size;
    
    [self.backgroundImageView removeFromSuperview];
    
    self.backgroundImageResized = [self imageWithImage:self.backgroundImageOriginal convertToSize:self.view.frame.size];
    self.backgroundImageView = [[UIImageView alloc] initWithImage:self.backgroundImageResized];
    [self.view addSubview:self.backgroundImageView];
    [self.view sendSubviewToBack:self.backgroundImageView];
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        [self setElementsPortrait];
    else
        [self setElementsLandscape];
}


#pragma mark UITextFieldDelegate methods


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.loginTextField) {
        [textField resignFirstResponder];
        [self.passwordTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    
    if(textField.returnKeyType == UIReturnKeyDone)
        [self doneClicked:self.loginButton];
    
    return YES;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger) buttonIndex {
    if (buttonIndex == 0) {
        [self.HUD removeFromSuperview];
    }
}


@end
