//
//  SimpleTableCell.h
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 02/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleTableCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *eventName;
@property (nonatomic, weak) IBOutlet UILabel *eventDevice;
@property (nonatomic, weak) IBOutlet UILabel *eventDate;

@end
