//
//  EventDetailTableViewController.m
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 02/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "EventDetailTableViewController.h"

@interface EventDetailTableViewController ()

@end

@implementation EventDetailTableViewController
@synthesize myTableView = _myTableView;
@synthesize event = _event;
@synthesize backgroundImageOriginal = _backgroundImageOriginal;
@synthesize backgroundImageResized = _backgroundImageResized;


- (void)viewDidLoad
{
    [super viewDidLoad];

    
    NSString *eventName = @"";
    NSString *eventDetail = @"";
    NSString *deviceCode = @"";
    NSString *deviceType = @"";
    if (self.event.event) {
        eventName = self.event.event.eventName;
        eventDetail = self.event.event.eventDescription;
    }
    if (self.event.device) {
        deviceCode = self.event.device.code;
        deviceType = self.event.device.deviceType;
    }
    
    NSString *latitude = [NSString stringWithFormat:@"%@", self.event.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%@", self.event.longitude];
    NSString *date = [NSString stringWithFormat:@"%@", self.event.metricDate];
    
    titleEventLabelItems = [NSArray arrayWithObjects: @"Evento", @"Descrição", nil];
    descriptionEventLabelItems = [NSArray arrayWithObjects:eventName, eventDetail, nil];
    titleDeviceLabelItems = [NSArray arrayWithObjects:@"Rastreador", @"Modelo", nil];
    descriptionDeviceLabelItems = [NSArray arrayWithObjects:deviceCode, deviceType, nil];
    titleEventHistoryLabelItems = [NSArray arrayWithObjects:@"Data", @"Latitude", @"Longitude", nil];
    descriptionEventHistoryLabelItems = [NSArray arrayWithObjects:date, latitude, longitude, nil];
    
    headerLabel = [[UILabel alloc] init];
    headerLabel.font = [UIFont boldSystemFontOfSize:12.0];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.backgroundImageOriginal = [UIImage imageNamed:@"bg.png"];
    self.backgroundImageResized = [self imageWithImage:self.backgroundImageOriginal convertToSize:self.view.frame.size];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:self.backgroundImageResized];
    [self.myTableView setBackgroundView:backgroundImageView];
}

- (void)viewDidUnload
{
    [self setMyTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    self.backgroundImageResized = [self imageWithImage:self.backgroundImageOriginal convertToSize:self.view.frame.size];
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30.0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    if (section == 0)
        return @"Evento";
    else if (section == 1)
        return @"Veículo";
    else
        return @"Gerais";
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    
    if (sectionTitle == nil) {
        return nil;
    }
    
    // Create label with section title
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 5.5f, 300.0f, 30.0f)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:18];
    label.textColor = [UIColor whiteColor];
    label.shadowColor = [UIColor blackColor];
    label.shadowOffset = CGSizeMake(0.0, 2.0);
    label.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    label.text = sectionTitle;
    
    // Create header view and add label as a subview
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 44.0f)];
    view.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    [view addSubview:label];
    
    return view;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0)
        return titleEventLabelItems.count;
    else if (section == 1)
        return titleDeviceLabelItems.count;
    else
        return titleEventHistoryLabelItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle
                                 reuseIdentifier:CellIdentifier];
    
    if (indexPath.section == 0) {
        cell.textLabel.text = [titleEventLabelItems objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [descriptionEventLabelItems objectAtIndex:indexPath.row];
    }
    
    if (indexPath.section == 1) {
        cell.textLabel.text = [titleDeviceLabelItems objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [descriptionDeviceLabelItems objectAtIndex:indexPath.row];
    }
    
    if (indexPath.section == 2){
        cell.textLabel.text = [titleEventHistoryLabelItems objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [descriptionEventHistoryLabelItems objectAtIndex:indexPath.row];
    }
    [[cell textLabel] setNumberOfLines:0]; // unlimited number of lines
    [[cell textLabel] setLineBreakMode:NSLineBreakByWordWrapping];
    
    [[cell detailTextLabel] setNumberOfLines:0]; // unlimited number of lines
    [[cell detailTextLabel] setLineBreakMode:NSLineBreakByWordWrapping];
    [[cell detailTextLabel] setFont:[UIFont systemFontOfSize: 14.0]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellTextTitle;
    NSString *cellTextSubtitle;
    UIFont *cellFont = [UIFont fontWithName:@"Helvetica" size:14.0];
    CGSize constraintSize = CGSizeMake(280.0f, MAXFLOAT);
    
    if (indexPath.section == 0) {
        cellTextTitle = [titleEventLabelItems objectAtIndex:indexPath.row];
        cellTextSubtitle = [descriptionEventLabelItems objectAtIndex:indexPath.row];
        
    }
    else if(indexPath.section == 1){
        cellTextTitle = [titleDeviceLabelItems objectAtIndex:indexPath.row];
        cellTextSubtitle = [descriptionDeviceLabelItems objectAtIndex:indexPath.row];
    }
    
    else{
        cellTextTitle = [titleEventHistoryLabelItems objectAtIndex:indexPath.row];
        cellTextSubtitle = [descriptionEventHistoryLabelItems objectAtIndex:indexPath.row];
    }
    
    CGSize titleLabelSize = [cellTextTitle sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
    CGSize subtitleLabelSize = [cellTextSubtitle sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat finalSize = titleLabelSize.height + subtitleLabelSize.height;
    
    return finalSize + 20;
    
}

#pragma mark - Table view delegate



@end
