//
//  MapPoint.h
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 28/09/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "Devices.h"
#import "EventHistoryDetail.h"

@interface MapPoint : NSObject <MKAnnotation>{
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, retain) Devices *device;
@property (nonatomic, retain) EventHistoryDetail *event;

-(id) initWithCoordinate: (CLLocationCoordinate2D) c title:(NSString *) t subtitle: (NSString *) sub device: (Devices *) d;
-(id) initWithCoordinate: (CLLocationCoordinate2D) c title:(NSString *) t subtitle: (NSString *) sub event: (EventHistoryDetail *) e;

@end
