//
//  Devices.m
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 01/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "Devices.h"
#import "EventHistoryDetail.h"
#import "LastPosition.h"


@implementation Devices

@dynamic code;
@dynamic deviceDescription;
@dynamic deviceType;
@dynamic deviceEnable;
@dynamic deviceId;
@dynamic deviceName;
@dynamic lastPosition;
@dynamic eventHistoryDetail;

@end
