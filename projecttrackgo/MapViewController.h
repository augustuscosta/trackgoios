//
//  MapViewController.h
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 26/09/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <Foundation/Foundation.h>
#import "Devices.h"
#import "LastPosition.h"
#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>

@interface MapViewController : UIViewController{
    NSArray *objects;
    Devices *pinDevice;
}

@property (strong, nonatomic) IBOutlet UIBarButtonItem *logoutButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *refreshButton;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) RKObjectManager* objectManager;

@end
