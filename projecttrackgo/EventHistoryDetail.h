//
//  EventHistoryDetail.h
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 01/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Devices, Event;

@interface EventHistoryDetail : NSManagedObject

@property (nonatomic, retain) NSNumber * eventHistoryId;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSDate * metricDate;
@property (nonatomic, retain) Event *event;
@property (nonatomic, retain) Devices *device;

@end
