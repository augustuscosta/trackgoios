//
//  SimpleTableCell.m
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 02/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "SimpleTableCell.h"

@implementation SimpleTableCell
@synthesize eventName = _eventName;
@synthesize eventDevice = _eventDevice;
@synthesize eventDate = _eventDate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.backgroundColor = [UIColor blackColor];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
