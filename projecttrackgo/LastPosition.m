//
//  LastPosition.m
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 01/10/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import "LastPosition.h"
#import "Devices.h"


@implementation LastPosition

@dynamic altitude;
@dynamic course;
@dynamic date;
@dynamic ignition;
@dynamic latitude;
@dynamic longitude;
@dynamic moving;
@dynamic panic;
@dynamic speed;
@dynamic device;

@end
