//
//  Token.h
//  projecttrackgo
//
//  Created by Augustus Costa on 1/27/14.
//  Copyright (c) 2014 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Token : NSManagedObject

@property (nonatomic, retain) NSString * token;

@end
