//
//  Utility.h
//  projecttrackgo
//
//  Created by Nichollas Fonseca on 26/09/12.
//  Copyright (c) 2012 Nichollas Fonseca. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject

+(BOOL)alreadyHasToken;

@end

